import { Field, ID, InputType } from '@nestjs/graphql';
import {
  Column,
  CreateDateColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { IsPassportNumber, IsString } from 'class-validator';

export class UpdateUsersDto {
  id: number;
  @IsString()
  email: string;
  @IsString({})
  password: string;
  lastAuth: Date;
}
