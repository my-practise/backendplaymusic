import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Post,
  Redirect,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { UserService } from '../services/user.service';
import { CreateUsersDto } from '../dto/create-users.dto';
import { AuthDto } from '../dto/auth.dto';
import { UpdateUsersDto } from '../dto/update-users.dto';
import { UserEntity } from '../entities/user.entity';
import { JwtAuthGuard } from '../guard/jwt.guard';

@Controller('user')
export class AuthController {
  constructor(private readonly userService: UserService) {}

  @UsePipes(new ValidationPipe())
  @Post()
  async registerByEmail(@Body() dto: CreateUsersDto) {
    const userExists = await this.userService.findUserByEmail(dto.email);
    if (!userExists) {
      return this.userService.saveUser(dto);
    }
    throw new BadRequestException('Этот е-майл существует');
  }

  @UsePipes(new ValidationPipe())
  @Post('register')
  async register(@Body() dto: UpdateUsersDto) {
    const userExists = await this.userService.findUserByEmail(dto.email);
    if (!userExists) {
      throw new BadRequestException('Такой пользователь не был найден');
    }
    if (userExists.isRegis != true) {
      return this.userService.updateUser(dto);
    }
    const user = await this.userService.validate(dto.email, dto.password);
    return this.userService.login(user.email);
  }

  @Post('admin')
  async createAdmin(@Body() dto: AuthDto) {
    return this.userService.createAdmin(dto);
  }

  @UsePipes(new ValidationPipe())
  @Post('login')
  @Redirect('page')
  @HttpCode(200)
  async login(@Body() dto: AuthDto) {
    const user = await this.userService.validate(dto.email, dto.password);
    return this.userService.login(user.email);
  }

  @Get('page')
  async getRegisUser(): Promise<UserEntity[]> {
    const regisUsers = this.userService.getRegisUsers();
    return regisUsers;
  }
}
