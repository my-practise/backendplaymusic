import {
  Body,
  Controller,
  HttpCode,
  Post,
  Redirect,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { AuthDto } from '../user/dto/auth.dto';
import { UserService } from '../user/services/user.service';

@Controller('admin')
export class AdminController {
  constructor(private readonly userService: UserService) {}
  @Post('admin')
  async createAdmin(@Body() dto: AuthDto) {
    return this.userService.createAdmin(dto);
  }

  @UsePipes(new ValidationPipe())
  @Post('admin/login')
  @Redirect('page')
  @HttpCode(200)
  async login(@Body() dto: AuthDto) {
    const user = await this.userService.validate(dto.email, dto.password);
    return this.userService.login(user.email);
  }
}
